﻿using UnityEngine;
using System.Collections;

public class ClickThis : MonoBehaviour {
    public PlayerSync PS;
    public int thisIndex;
    ChatSystem CS;
    
    void Start()
    {
        CS = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ChatSystem>();
    }
    
    void OnMouseDown()
    {
        Debug.Log(CS.username);
        PS.DestroyThisTic(thisIndex);
    }
}
