﻿using UnityEngine;
using System.Collections;

	/// <summary>
	/// This script is attached to the player and it
	/// ensures that every players position, rotation, and scale,
	/// are kept up to date across the network.
	///
	/// This script is closely based on a script written by M2H,
	/// and edited by AnthonyB28 ( https://github.com/AnthonyB28/FPS_Unity/blob/master/MovementUpdate.cs )
	/// </summary>
    
public class PlayerSync : MonoBehaviour {

	Vector3 lastPosition;
	Quaternion lastRotation;
	Transform myTransform;
	
	Vector3 targetPosition;
	Quaternion targetRotation;
	
	[SerializeField] float posThreshold = 0.1f;
	[SerializeField] float rotThreshold = 5f;

    public int[] arrTicTacToe = new int[9]{0,0,0,0,0,0,0,0,0};
    public GameObject[] arrTictactoe;
    public int IntToDestroy;
    public ChatSystem CS;
	void Start ()
	{
        CS = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ChatSystem>();
		if(networkView.isMine == true)
		{
		    myTransform = transform;
			//Make sure everyone sees the player at right location the moment he spawns 
			if(!Network.isServer)
			{
				networkView.RPC("UpdateMovement", RPCMode.OthersBuffered, myTransform.position, myTransform.rotation);
	    	}
	    }
	}
	
	[RPC]
	void SetUsername (string name)
	{
		gameObject.name = name;
	}
	
	
	void Update ()
	{
        

        
	    if (networkView.isMine)
	    {
		    SendMovement();
	    }
	    else
	    {
		    ApplyMovement();
	    }
	}
	
	void SendMovement()
	{
		if (Vector3.Distance(myTransform.position, lastPosition) >= posThreshold)
		{
			//If player has moved, send move update to other players
			//Capture the player's position before the RPC is fired off and use this
			//to determine if the player has moved in the if statement above.
			lastPosition = transform.position;
			networkView.RPC("UpdateMovement", RPCMode.OthersBuffered, myTransform.position, myTransform.rotation);
		}
		if (Quaternion.Angle(myTransform.rotation, lastRotation) >= rotThreshold)
		{
			//Capture the player's rotation before the RPC is fired off and use this
			//to determine if the player has turned in the if statement above. 
			lastRotation = transform.rotation;
			networkView.RPC("UpdateMovement", RPCMode.OthersBuffered, myTransform.position, myTransform.rotation);
		}
	}
	
	void ApplyMovement ()
	{
		transform.position = Vector3.Lerp(transform.position, targetPosition, 0.5f);
		transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.5f);
	}
	
	[RPC]
	void UpdateMovement (Vector3 newPosition, Quaternion newRotation)
	{
		targetPosition = newPosition;
		targetRotation = newRotation;
	}


    [RPC]
    public void UpdateArrayTictactoe(int IntToDestroy, string UserName)
    {
        Debug.Log(UserName);
        if(arrTictactoe[IntToDestroy] != null)
        Network.Destroy(arrTictactoe[IntToDestroy]);
    }

    public void DestroyThisTic(int index)
    {
        IntToDestroy = index;
        networkView.RPC("UpdateArrayTictactoe", RPCMode.AllBuffered, IntToDestroy, CS.username);
    }


    void CheckIfWin()
    {
        if (arrTicTacToe[0] != 0 && arrTicTacToe[0] == arrTicTacToe[1] && arrTicTacToe[1] == arrTicTacToe[2])
        {
            //Win straight
        }
        else if (arrTicTacToe[0] != 0 && arrTicTacToe[0] == arrTicTacToe[3] && arrTicTacToe[3] == arrTicTacToe[6])
        {
            //Win 2
        }
        else if (arrTicTacToe[0] != 0 && arrTicTacToe[0] == arrTicTacToe[4] && arrTicTacToe[4] == arrTicTacToe[8])
        {

        }
        else if (arrTicTacToe[2] != 0 && arrTicTacToe[2] == arrTicTacToe[4] && arrTicTacToe[4] == arrTicTacToe[6])
        {

        }
        else if (arrTicTacToe[2] != 0 && arrTicTacToe[2] == arrTicTacToe[5] && arrTicTacToe[5] == arrTicTacToe[8])
        {

        }
        else if (arrTicTacToe[3] != 0 && arrTicTacToe[3] == arrTicTacToe[4] && arrTicTacToe[4] == arrTicTacToe[5])
        {

        }
        else if (arrTicTacToe[6] != 0 && arrTicTacToe[6] == arrTicTacToe[7] && arrTicTacToe[7] == arrTicTacToe[8])
        {

        }

    }
}